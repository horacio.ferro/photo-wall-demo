import boto3
import json
import os
import random
import time
import uuid
from PIL import Image

PREFIX = 'processed/'
""" Prefix for processed images. """
MAX_VALUE = 9223372036854775807
""" Maximum long value for reverse timestamp. """


def handler(event, context):
    """
    Lambda function handler.

    :param event: Contains the file uploaded to the bucket.
    :param context: Execution context.
    """
    for record in event['Records']:
        bucket = record['s3']['bucket']['name']
        key = record['s3']['object']['key']
        print('Resizing image {} on bucket {}.'.format(key, bucket))
        new_key = resize_image(bucket, key)
        print('Sending to Rekognition image {} on bucket {}.'.format(new_key, bucket))
        faces_response, mod_response = process_image(bucket, new_key)
        write_record(faces_response, mod_response, new_key)


def resize_image(bucket, key):
    """
    Check the image current size, if its bigger than expected its resized and uploaded to the destination bucket,
    otherwise the image is uploaded without any modification.

    :param bucket: S3 bucket of the original image.
    :param key: S3 object key of the original image.
    :return: Tuple of new bucket and key.
    """
    max_width = int(os.environ['MAX_WIDTH'])
    max_height = int(os.environ['MAX_HEIGHT'])
    client = boto3.client('s3')
    local_file = '/tmp/' + str(uuid.uuid4()) + key[key.rfind('.'):]
    new_key = PREFIX + str(uuid.uuid4()) + key[key.rfind('.'):]
    client.download_file(Bucket=bucket, Key=key, Filename=local_file)
    with open(local_file, 'r+b') as file:
        with Image.open(file) as image:
            width, height = image.size
            ratio = min(max_width / width, max_height / height)
            if ratio < 1:
                filename = str(uuid.uuid4()) + key[key.rfind('.'):]
                old_file = local_file
                local_file = '/tmp/' + filename
                size = int(width * ratio), int(height * ratio)
                image.thumbnail(size, Image.ANTIALIAS)
                image.save(local_file, image.format)
                os.remove(old_file)
    client.upload_file(Bucket=bucket, Key=new_key, Filename=local_file)
    os.remove(local_file)
    return new_key


def process_image(bucket, key):
    """
    Process the image to detect faces.

    :param bucket: S3 bucket containing the image to process.
    :param key: The image key.
    :return: The response from rekogniton.
    """
    client = boto3.client('rekognition')
    image = {'S3Object': {'Bucket': bucket, 'Name': key}}
    faces_response = client.detect_faces(Image=image, Attributes=['DEFAULT', 'ALL'])
    mod_response = client.detect_moderation_labels(Image=image)
    return faces_response, mod_response


def write_record(faces, moderation, key):
    """
    Inserts the data into DynamoDB.

    :param faces: Outcome of the detect faces.
    :param moderation: Outcome of the detect moderation labels.
    :param key: Of the photo in the S3 bucket.
    """
    client = boto3.client('dynamodb')
    table = os.environ['DYNAMO_TABLE'] if 'DYNAMO_TABLE' in os.environ else 'uploaded_photos'
    faces = retrieve_faces(faces)
    add_labels(faces, moderation)
    reverse_time = MAX_VALUE - int(round(time.time() * 1000))
    faces['s3_key'] = {'S': key}
    faces['timestamp'] = {'N': str(reverse_time)}
    faces['group'] = {'N': str(random.randrange(1, 10, 1))}
    faces['original_response'] = {'S': json.dumps(faces)}
    print(table)
    client.put_item(TableName=table, Item=faces)


def retrieve_faces(data):
    """
    Retrieve attributes from the response of detect faces.

    :param data: The outcome of the detect faces call.
    :return: Dictionary with data for DynamoDB.
    """
    faces = {
        'Smile': {'L': list()},
        'Gender': {'L': list()},
        'Sentiment': {'L': list()}
    }
    for face in data['FaceDetails']:
        faces['Smile']['L'].append({'BOOL': face['Smile']['Value']})
        faces['Gender']['L'].append({'S': face['Gender']['Value']})
        faces['Sentiment']['L'].append({'S': get_emotion(face['Emotions'])})
    return faces


def get_emotion(emotions):
    """
    Retrieves the emotion with more confidence from the result set.

    :param emotions: The list of emotions for the given face.
    :return: The predominant emotion according to the confidence.
    """
    confidence = 0.0
    emotion_type = 'None'
    for emotion in emotions:
        if emotion['Confidence'] > confidence:
            confidence = emotion['Confidence']
            emotion_type = emotion['Type']
    return emotion_type


def add_labels(faces, moderation):
    """
    Add the moderation labels found in the photo if any.

    :param faces: Data from the photo.
    :param moderation: The outcome of the moderation labels method.
    """
    faces['moderation_labels'] = {'L': list()}
    for label in moderation['ModerationLabels']:
        faces['moderation_labels']['L'].append({'S': label['Name']})
